package ru.domozavr.core.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.domozavr.core.domain.dto.CreateUserDto;
import ru.domozavr.core.domain.dto.UpdateUserRoleDto;
import ru.domozavr.core.domain.dto.UserDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.domain.entity.UserEntity;
import ru.domozavr.core.exception.NotFoundException;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.mapper.UserMapper;
import ru.domozavr.core.repository.UserRepository;

@ContextConfiguration(classes = {UserService.class})
@ExtendWith(SpringExtension.class)
public class UserServiceTest {
  @MockBean
  private PasswordEncoder passwordEncoder;

  @MockBean
  private UserMapper userMapper;

  @MockBean
  private UserRepository userRepository;

  @Autowired
  private UserService userService;

  @Test
  public void testGetAll() {
    when(this.userRepository.findAll((org.springframework.data.domain.Pageable) any()))
        .thenThrow(new ValidationException(ServiceError.GENERIC_ERROR));
    assertThrows(ValidationException.class, () -> this.userService.getAll(null));
    verify(this.userRepository).findAll((org.springframework.data.domain.Pageable) any());
  }

  @Test
  public void testCreateUser() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    when(this.userRepository.save((UserEntity) any())).thenReturn(userEntity);

    UserEntity userEntity1 = new UserEntity();
    userEntity1.setEmail("jane.doe@example.org");
    userEntity1.setPassword("iloveyou");
    userEntity1.setId(null);
    userEntity1.setRoles(null);
    userEntity1.setLastToken("ABC123");
    UserDto userDto = new UserDto();
    when(this.userMapper.convert((UserEntity) any())).thenReturn(userDto);
    when(this.userMapper.convert((CreateUserDto) any())).thenReturn(userEntity1);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
    assertSame(userDto, this.userService.createUser(new CreateUserDto("jane.doe@example.org", "iloveyou")));
    verify(this.userRepository).save((UserEntity) any());
    verify(this.userMapper).convert((CreateUserDto) any());
    verify(this.userMapper).convert((UserEntity) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
  }

  @Test
  public void testUpdateUserRoles() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    UserEntity userEntity1 = new UserEntity();
    userEntity1.setEmail("jane.doe@example.org");
    userEntity1.setPassword("iloveyou");
    userEntity1.setId(null);
    userEntity1.setRoles(null);
    userEntity1.setLastToken("ABC123");
    Optional<UserEntity> ofResult = Optional.<UserEntity>of(userEntity);
    when(this.userRepository.save((UserEntity) any())).thenReturn(userEntity1);
    when(this.userRepository.findById((UUID) any())).thenReturn(ofResult);
    UserDto userDto = new UserDto();
    when(this.userMapper.convert((UserEntity) any())).thenReturn(userDto);

    assertSame(userDto, this.userService.updateUserRoles(new UpdateUserRoleDto()));
    verify(this.userRepository).findById((UUID) any());
    verify(this.userRepository).save((UserEntity) any());
    verify(this.userMapper).convert((UserEntity) any());
  }

  @Test
  public void testUpdateUserRoles2() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    when(this.userRepository.save((UserEntity) any())).thenReturn(userEntity);
    when(this.userRepository.findById((UUID) any())).thenReturn(Optional.<UserEntity>empty());
    when(this.userMapper.convert((UserEntity) any())).thenReturn(new UserDto());
    assertThrows(NotFoundException.class, () -> this.userService.updateUserRoles(new UpdateUserRoleDto()));
    verify(this.userRepository).findById((UUID) any());
  }

  @Test
  public void testIsExistsByEmail() {
    when(this.userRepository.existsByEmailIgnoreCase((String) any())).thenReturn(true);
    assertTrue(this.userService.isExistsByEmail("jane.doe@example.org"));
    verify(this.userRepository).existsByEmailIgnoreCase((String) any());
  }

  @Test
  public void testIsExistsByEmail2() {
    when(this.userRepository.existsByEmailIgnoreCase((String) any())).thenReturn(false);
    assertFalse(this.userService.isExistsByEmail("jane.doe@example.org"));
    verify(this.userRepository).existsByEmailIgnoreCase((String) any());
  }

  @Test
  public void testIsExistsByEmailAndPassword() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    Optional<UserEntity> ofResult = Optional.<UserEntity>of(userEntity);
    when(this.userRepository.findByEmailIgnoreCase((String) any())).thenReturn(ofResult);
    when(this.passwordEncoder.matches((CharSequence) any(), (String) any())).thenReturn(true);
    assertTrue(this.userService.isExistsByEmailAndPassword("jane.doe@example.org", "iloveyou"));
    verify(this.userRepository).findByEmailIgnoreCase((String) any());
    verify(this.passwordEncoder).matches((CharSequence) any(), (String) any());
  }

  @Test
  public void testIsExistsByEmailAndPassword2() {
    when(this.userRepository.findByEmailIgnoreCase((String) any())).thenReturn(Optional.<UserEntity>empty());
    when(this.passwordEncoder.matches((CharSequence) any(), (String) any())).thenReturn(true);
    assertThrows(ValidationException.class,
        () -> this.userService.isExistsByEmailAndPassword("jane.doe@example.org", "iloveyou"));
    verify(this.userRepository).findByEmailIgnoreCase((String) any());
  }

  @Test
  public void testIsExistsByEmailAndPassword3() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    Optional<UserEntity> ofResult = Optional.<UserEntity>of(userEntity);
    when(this.userRepository.findByEmailIgnoreCase((String) any())).thenReturn(ofResult);
    when(this.passwordEncoder.matches((CharSequence) any(), (String) any())).thenReturn(false);
    assertFalse(this.userService.isExistsByEmailAndPassword("jane.doe@example.org", "iloveyou"));
    verify(this.userRepository).findByEmailIgnoreCase((String) any());
    verify(this.passwordEncoder).matches((CharSequence) any(), (String) any());
  }

  @Test
  public void testInvalidateUserToken() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    UserEntity userEntity1 = new UserEntity();
    userEntity1.setEmail("jane.doe@example.org");
    userEntity1.setPassword("iloveyou");
    userEntity1.setId(null);
    userEntity1.setRoles(null);
    userEntity1.setLastToken("ABC123");
    Optional<UserEntity> ofResult = Optional.<UserEntity>of(userEntity);
    when(this.userRepository.save((UserEntity) any())).thenReturn(userEntity1);
    when(this.userRepository.findByEmailIgnoreCase((String) any())).thenReturn(ofResult);

    this.userService.invalidateUserToken("jane.doe@example.org");

    verify(this.userRepository).findByEmailIgnoreCase((String) any());
    verify(this.userRepository).save((UserEntity) any());
  }

  @Test
  public void testInvalidateUserToken2() {
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("jane.doe@example.org");
    userEntity.setPassword("iloveyou");
    userEntity.setId(null);
    userEntity.setRoles(null);
    userEntity.setLastToken("ABC123");
    when(this.userRepository.save((UserEntity) any())).thenReturn(userEntity);
    when(this.userRepository.findByEmailIgnoreCase((String) any())).thenReturn(Optional.<UserEntity>empty());
    assertThrows(NotFoundException.class, () -> this.userService.invalidateUserToken("jane.doe@example.org"));
    verify(this.userRepository).findByEmailIgnoreCase((String) any());
  }
}

