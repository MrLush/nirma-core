package ru.domozavr.core.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {AuthenticationService.class})
@ExtendWith(SpringExtension.class)
public class AuthenticationServiceTest {
  @Autowired
  private AuthenticationService authenticationService;

  @Test
  public void testIsUserAuthenticated() {
    assertFalse(this.authenticationService.isUserAuthenticated());
  }

  @Test
  public void testGetAuthenticatedUser() {
    assertThrows(AuthenticationServiceException.class, () -> this.authenticationService.getAuthenticatedUser());
  }
}

