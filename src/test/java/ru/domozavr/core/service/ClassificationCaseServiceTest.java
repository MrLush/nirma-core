package ru.domozavr.core.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.domozavr.core.domain.dto.ClassificationCaseDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.mapper.ClassificationCaseMapper;
import ru.domozavr.core.repository.ClassificationCaseRepository;
import ru.domozavr.core.security.AuthorisedUser;
import ru.domozavr.core.util.FileUtil;

@ContextConfiguration(classes = {ClassificationCaseService.class, AuthenticationService.class,
    ClassificationCaseMapper.class, FileUtil.class})
@ExtendWith(SpringExtension.class)
public class ClassificationCaseServiceTest {
  @MockBean
  private AuthenticationService authenticationService;

  @MockBean
  private ClassificationCaseMapper classificationCaseMapper;

  @MockBean
  private ClassificationCaseRepository classificationCaseRepository;

  @Autowired
  private ClassificationCaseService classificationCaseService;

  @MockBean
  private FileUtil fileUtil;

  @Test
  public void testAddClassificationCase() {
    when(this.fileUtil.getPojoFromCsv((Class<Object>) any(), (String) any()))
        .thenThrow(new ValidationException(ServiceError.GENERIC_ERROR));
    when(this.fileUtil.getDocumentPath((java.util.UUID) any(), (java.util.UUID) any())).thenReturn(null);
    when(this.authenticationService.getAuthenticatedUser()).thenReturn(new AuthorisedUser());
    assertThrows(ValidationException.class,
        () -> this.classificationCaseService.addClassificationCase(new ClassificationCaseDto()));
    verify(this.fileUtil).getDocumentPath((java.util.UUID) any(), (java.util.UUID) any());
    verify(this.fileUtil).getPojoFromCsv((Class<Object>) any(), (String) any());
    verify(this.authenticationService).getAuthenticatedUser();
  }

  @Test
  public void testAddClassificationCase2() {
    when(this.fileUtil.getPojoFromCsv((Class<Object>) any(), (String) any())).thenReturn(null);
    when(this.fileUtil.getDocumentPath((UUID) any(), (UUID) any())).thenReturn(null);
    AuthorisedUser authorisedUser = mock(AuthorisedUser.class);
    when(authorisedUser.getId()).thenThrow(new ValidationException(ServiceError.GENERIC_ERROR));
    when(this.authenticationService.getAuthenticatedUser()).thenReturn(authorisedUser);
    assertThrows(ValidationException.class,
        () -> this.classificationCaseService.addClassificationCase(new ClassificationCaseDto()));
    verify(this.authenticationService).getAuthenticatedUser();
    verify(authorisedUser).getId();
  }
}

