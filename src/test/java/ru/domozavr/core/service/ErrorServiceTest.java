package ru.domozavr.core.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.domozavr.core.configuration.properties.PathsProperties;
import ru.domozavr.core.domain.dto.ErrorsDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ServerException;
import ru.domozavr.core.security.AuthorisedUser;
import ru.domozavr.core.util.FileUtil;
import ru.domozavr.core.util.TerminalUtil;

@ContextConfiguration(classes = {ErrorService.class, PathsProperties.class})
@ExtendWith(SpringExtension.class)
public class ErrorServiceTest {
  @MockBean
  private AuthenticationService authenticationService;

  @Autowired
  private ErrorService errorService;

  @MockBean
  private FileUtil fileUtil;

  @Autowired
  private PathsProperties pathsProperties;

  @MockBean
  private TerminalUtil terminalUtil;

  @Test
  public void testGetLineErrors() {
    when(this.authenticationService.getAuthenticatedUser()).thenThrow(new ServerException(ServiceError.GENERIC_ERROR));
    assertThrows(ServerException.class,
        () -> this.errorService.getLineErrors(new ErrorsDto("42", "Standard", "42", "Classified Class")));
    verify(this.authenticationService).getAuthenticatedUser();
  }

  @Test
  public void testGetLineErrors2() {
    AuthorisedUser authorisedUser = mock(AuthorisedUser.class);
    when(authorisedUser.getId()).thenThrow(new ServerException(ServiceError.GENERIC_ERROR));
    when(this.authenticationService.getAuthenticatedUser()).thenReturn(authorisedUser);
    assertThrows(ServerException.class,
        () -> this.errorService.getLineErrors(new ErrorsDto("42", "Standard", "42", "Classified Class")));
    verify(this.authenticationService).getAuthenticatedUser();
    verify(authorisedUser).getId();
  }

  @Test
  public void testGetDocumentErrors() {
    when(this.authenticationService.getAuthenticatedUser()).thenThrow(new ServerException(ServiceError.GENERIC_ERROR));
    assertThrows(ServerException.class,
        () -> this.errorService.getDocumentErrors(new ErrorsDto("42", "Standard", "42", "Classified Class")));
    verify(this.authenticationService).getAuthenticatedUser();
  }

  @Test
  public void testGetDocumentErrors2() {
    AuthorisedUser authorisedUser = mock(AuthorisedUser.class);
    when(authorisedUser.getId()).thenThrow(new ServerException(ServiceError.GENERIC_ERROR));
    when(this.authenticationService.getAuthenticatedUser()).thenReturn(authorisedUser);
    assertThrows(ServerException.class,
        () -> this.errorService.getDocumentErrors(new ErrorsDto("42", "Standard", "42", "Classified Class")));
    verify(this.authenticationService).getAuthenticatedUser();
    verify(authorisedUser).getId();
  }
}

