package ru.domozavr.core.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import javax.servlet.http.HttpServletRequestWrapper;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.connector.Request;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.domozavr.core.domain.dto.CreateUserDto;
import ru.domozavr.core.domain.dto.LoginDto;
import ru.domozavr.core.domain.dto.UserDto;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.security.JwtProvider;

@ContextConfiguration(classes = {AuthService.class, JwtProvider.class, UserService.class})
@ExtendWith(SpringExtension.class)
public class AuthServiceTest {
  @Autowired
  private AuthService authService;

  @MockBean
  private JwtProvider jwtProvider;

  @MockBean
  private UserService userService;

  @Test
  public void testRegister() {
    when(this.userService.isExistsByEmail(any())).thenReturn(true);
    assertThrows(ValidationException.class,
        () -> this.authService.register(new CreateUserDto("jane.doe@example.org", "iloveyou")));
  }

  @Test
  public void testRegister2() {
    when(this.userService.createUser(any())).thenReturn(new UserDto());
    when(this.userService.isExistsByEmail(any())).thenReturn(false);
    when(this.jwtProvider.generateToken(any())).thenReturn("foo");
    assertEquals("foo", this.authService.register(new CreateUserDto("jane.doe@example.org", "iloveyou")));
  }

  @Test
  public void testLogin() {
    when(this.userService.isExistsByEmailAndPassword(any(), any())).thenReturn(true);
    when(this.jwtProvider.generateToken(any())).thenReturn("foo");
    assertEquals("foo", this.authService.login(new LoginDto("jane.doe@example.org", "iloveyou")));
  }

  @Test
  public void testLogin2() {
    when(this.userService.isExistsByEmailAndPassword(any(), any())).thenReturn(false);
    when(this.jwtProvider.generateToken(any())).thenReturn("foo");
    assertThrows(ValidationException.class,
        () -> this.authService.login(new LoginDto("jane.doe@example.org", "iloveyou")));
  }

  @Test
  public void testLogin3() {
    when(this.userService.isExistsByEmailAndPassword(any(), any())).thenReturn(true);
    when(this.jwtProvider.generateToken(any())).thenReturn("foo");
    LoginDto loginDto = mock(LoginDto.class);
    when(loginDto.getPassword()).thenReturn("foo");
    when(loginDto.getEmail()).thenReturn("foo");
    assertEquals("foo", this.authService.login(loginDto));
    verify(loginDto, atLeast(1)).getEmail();
    verify(loginDto).getPassword();
  }

  @Test
  public void testLogout() {
    doNothing().when(this.userService).invalidateUserToken(any());
    when(this.jwtProvider.getSubFromToken(any())).thenReturn("foo");
    when(this.jwtProvider.getTokenFromRequest(any()))
        .thenReturn(Optional.<String>of("foo"));
    this.authService.logout(new HttpServletRequestWrapper(new HttpServletRequestWrapper(
        new HttpServletRequestWrapper(new HttpServletRequestWrapper(new Request(new Connector()))))));
    verify(this.userService).invalidateUserToken(any());
    verify(this.jwtProvider).getSubFromToken(any());
    verify(this.jwtProvider).getTokenFromRequest(any());
  }
}

