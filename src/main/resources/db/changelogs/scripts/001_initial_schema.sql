create schema if not exists norm_control;

set search_path = norm_control, public;

CREATE TABLE user_table (
    id uuid PRIMARY KEY,
    email text NOT NULL,
    password text NOT NULL
);
CREATE TABLE user_roles (
    user_id uuid NOT NULL ,
    role text NOT NULL,
    FOREIGN KEY(user_id)
    REFERENCES user_table(id)
);
CREATE TABLE user_tokens (
    user_id uuid NOT NULL ,
    token text NOT NULL,
    FOREIGN KEY(user_id)
    REFERENCES user_table(id)
);
