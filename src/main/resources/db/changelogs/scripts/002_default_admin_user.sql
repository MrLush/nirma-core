set search_path = norm_control, public;

INSERT INTO user_table (id, email, password)
VALUES (
    '500e9e72-46dd-4ed6-81dc-85748dc51d82',
    'admin@admin.com',
    '$2a$10$x/oggDTuKejfVZnmQM6mZeXuBkrpNOYBdpcRo1GCYT1.LuH/gDe/e'
);

INSERT INTO user_roles (user_id, role)
VALUES ('500e9e72-46dd-4ed6-81dc-85748dc51d82', 'ADMIN'),
       ('500e9e72-46dd-4ed6-81dc-85748dc51d82', 'USER');

INSERT INTO user_tokens (user_id, token)
VALUES ('500e9e72-46dd-4ed6-81dc-85748dc51d82', 'undefined');