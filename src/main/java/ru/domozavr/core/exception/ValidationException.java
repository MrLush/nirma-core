package ru.domozavr.core.exception;

import ru.domozavr.core.domain.dto.error.ServiceError;

public class ValidationException extends ServiceException {

  public ValidationException(ServiceError error) {
    super(error);
  }

  public ValidationException(ServiceError error, String message) {
    super(error, message);
  }

  public ValidationException(ServiceError error, String message, Throwable cause) {
    super(error, message, cause);
  }
}
