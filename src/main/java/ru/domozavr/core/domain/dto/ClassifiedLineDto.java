package ru.domozavr.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ClassifiedLineDto {
  @JsonProperty("paragraph-id")
  private String paragraphId;
  @JsonProperty("standard-word-predict")
  private String standardWordPredict;
  @JsonProperty("gosts-predict")
  private String gostsPredict;
  @JsonProperty("cur-element-mark")
  private String curElementMark;
  @JsonProperty("predicates-group")
  private String predicatesGroup;
  @JsonProperty("checked")
  private Boolean checked;
  @JsonProperty("final-class")
  private String finalClass;
}
