package ru.domozavr.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ErrorsDto {
  @JsonProperty("paragraph-id")
  private String paragraphId;
  @NotNull
  @JsonProperty("standard")
  private String standard;
  @NotNull
  @JsonProperty("document-id")
  private String documentId;
  @JsonProperty("classified-class")
  private String classifiedClass;
}
