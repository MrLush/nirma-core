package ru.domozavr.core.domain.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {

  @NotNull
  private String email;

  @NotNull
  private String password;
}
