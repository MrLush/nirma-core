package ru.domozavr.core.domain.dto;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.domozavr.core.security.Role;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

  private UUID id;

  private String email;

  private String password;

  private List<Role> roles;

  private String lastToken;

}
