package ru.domozavr.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.domozavr.core.security.Role;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserRoleDto {

  @NotNull
  @JsonProperty("id")
  private UUID id;

  @NotNull
  @JsonProperty("roles")
  @NotEmpty
  private List<Role> roles;
}
