package ru.domozavr.core.domain.dto.error;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ServiceError {

  GENERIC_ERROR("error.generic.error"),

  // type - validation
  CANNOT_BE_EDITED("error.cannot.be.edited"),
  INVALID_REQUEST("error.invalid.request"),
  VALIDATION_FAILED("error.validation.failed"),
  ENCODING_EXCEPTION("error.encoding.exception"),
  USER_ROLE_EXIST("error.user.role.exist"),
  REQUEST_EXIST("error.request.exist"),
  RESPONSE_EXIST("error.response.exist"),
  WRONG_CREDENTIALS("error.wrong.credentials"),
  EMPTY_TOKEN_IN_REQUEST("error.empty.token.in.request"),
  WRONG_FILE_EXTENSION("error.wrong.file.extension"),
  FILE_NOT_FOUND("error.file.not.found"),
  LINE_NOT_FOUND("error.line.not.found"),

  // type - security
  ACCESS_DENIED("error.security.access.denied"),

  // type - request
  NO_SUCH_ENTITY("error.no.such.entity"),

  // type - server
  PARSE_ERROR("error.parse.error");

  @JsonValue
  private final String value;
}
