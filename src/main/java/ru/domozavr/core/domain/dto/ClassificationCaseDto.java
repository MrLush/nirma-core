package ru.domozavr.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassificationCaseDto {
  @NotNull
  @JsonProperty("paragraph-id")
  private String paragraphId;
  @NotNull
  @JsonProperty("classified-successfully")
  private boolean classifiedSuccessfully;
  @NotNull
  @JsonProperty("document-id")
  private UUID documentId;
  @NotNull
  @JsonProperty("classified-class")
  private String classifiedClass;
  @NotNull
  @JsonProperty("class-chosen-by-user")
  private String classChosenByUser;
}
