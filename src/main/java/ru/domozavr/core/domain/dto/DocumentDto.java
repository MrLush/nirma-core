package ru.domozavr.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class DocumentDto {

  private UUID id;
  @JsonProperty("document-path")
  private String documentPath;
  @JsonProperty("pre-classified-document")
  private List<ClassifiedLineDto> preClassifiedDocument;
}
