package ru.domozavr.core.domain.dto.error;

public enum ErrorType {

  SERVER,
  REQUEST,
  SECURITY,
  VALIDATION,
}
