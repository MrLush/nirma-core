package ru.domozavr.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.domozavr.core.validator.Password;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserDto {

  @NotNull
  @JsonProperty("email")
  @Email
  private String email;

  @NotNull
  @JsonProperty("password")
  @Password
  private String password;

}
