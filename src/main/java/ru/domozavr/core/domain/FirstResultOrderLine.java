package ru.domozavr.core.domain;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FirstResultOrderLine {

  @CsvBindByPosition(position = 0)
  private String id;
  @CsvBindByPosition(position = 1)
  private String content;
  @CsvBindByPosition(position = 2)
  private String specialSymbolsCount;
  @CsvBindByPosition(position = 3)
  private String wordsCount;
  @CsvBindByPosition(position = 4)
  private String symbolCount;
  @CsvBindByPosition(position = 5)
  private String lowercase;
  @CsvBindByPosition(position = 6)
  private String uppercase;
  @CsvBindByPosition(position = 7)
  private String lastSymbolPd;
  @CsvBindByPosition(position = 8)
  private String firstKeyWord;
  @CsvBindByPosition(position = 9)
  private String prevElementMark;
  @CsvBindByPosition(position = 10)
  private String curElementMark;
  @CsvBindByPosition(position = 11)
  private String nextElementMark;
  @CsvBindByPosition(position = 12)
  private String fullBold;
  @CsvBindByPosition(position = 13)
  private String fullItalic;
  @CsvBindByPosition(position = 14)
  private String alignment;
  @CsvBindByPosition(position = 15)
  private String keepLinesTogether;
  @CsvBindByPosition(position = 16)
  private String keepWithNext;
  @CsvBindByPosition(position = 17)
  private String leftIndentation;
  @CsvBindByPosition(position = 18)
  private String lineSpacing;
  @CsvBindByPosition(position = 19)
  private String noSpaceBetweenParagraphsOfSameStyle;
  @CsvBindByPosition(position = 20)
  private String outlineLevel;
  @CsvBindByPosition(position = 21)
  private String pageBreakBefore;
  @CsvBindByPosition(position = 22)
  private String rightIndentation;
  @CsvBindByPosition(position = 23)
  private String spaceAfter;
  @CsvBindByPosition(position = 24)
  private String spaceBefore;
  @CsvBindByPosition(position = 25)
  private String specialIndentation;
}
