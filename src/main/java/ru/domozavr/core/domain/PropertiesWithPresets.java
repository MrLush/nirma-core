package ru.domozavr.core.domain;

import com.opencsv.bean.CsvBindByPosition;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropertiesWithPresets {
  @CsvBindByPosition(position = 0)
  private String classesFromRequirements;
  @CsvBindByPosition(position = 1)
  private String classesFromWord;
  @CsvBindByPosition(position = 2)
  private String classesFromUser;
  @CsvBindByPosition(position = 3)
  private String listFormatIsList;
  @CsvBindByPosition(position = 4)
  private String listFormatListItem;
  @CsvBindByPosition(position = 5)
  private String listFormatLevel;
  @CsvBindByPosition(position = 6)
  private String listFormatAlignment;
  @CsvBindByPosition(position = 7)
  private String listFormatNumberFormat;
  @CsvBindByPosition(position = 8)
  private String listFormatNumberStyle;
  @CsvBindByPosition(position = 9)
  private String id;
  @CsvBindByPosition(position = 10)
  private String content;
  @CsvBindByPosition(position = 11)
  private String specialSymbolsCount;
  @CsvBindByPosition(position = 12)
  private String wordsCount;
  @CsvBindByPosition(position = 13)
  private String symbolCount;
  @CsvBindByPosition(position = 14)
  private String lowercase;
  @CsvBindByPosition(position = 15)
  private String uppercase;
  @CsvBindByPosition(position = 16)
  private String lastSymbolPd;
  @CsvBindByPosition(position = 17)
  private String firstKeyWord;
  @CsvBindByPosition(position = 18)
  private String prevElementMark;
  @CsvBindByPosition(position = 19)
  private String curElementMark;
  @CsvBindByPosition(position = 20)
  private String nextElementMark;
  @CsvBindByPosition(position = 21)
  private String fullBold;
  @CsvBindByPosition(position = 22)
  private String fullItalic;
  @CsvBindByPosition(position = 23)
  private String alignment;
  @CsvBindByPosition(position = 24)
  private String keepLinesTogether;
  @CsvBindByPosition(position = 25)
  private String keepWithNext;
  @CsvBindByPosition(position = 26)
  private String leftIndentation;
  @CsvBindByPosition(position = 27)
  private String lineSpacing;
  @CsvBindByPosition(position = 28)
  private String noSpaceBetweenParagraphsOfSameStyle;
  @CsvBindByPosition(position = 29)
  private String outlineLevel;
  @CsvBindByPosition(position = 30)
  private String pageBreakBefore;
  @CsvBindByPosition(position = 31)
  private String rightIndentation;
  @CsvBindByPosition(position = 32)
  private String spaceAfter;
  @CsvBindByPosition(position = 33)
  private String spaceBefore;
  @CsvBindByPosition(position = 34)
  private String specialIndentation;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PropertiesWithPresets that = (PropertiesWithPresets) o;
    return Objects.equals(classesFromRequirements, that.classesFromRequirements)
        && Objects.equals(classesFromWord, that.classesFromWord)
        && Objects.equals(classesFromUser, that.classesFromUser)
        && Objects.equals(listFormatIsList, that.listFormatIsList)
        && Objects.equals(listFormatListItem, that.listFormatListItem)
        && Objects.equals(listFormatLevel, that.listFormatLevel)
        && Objects.equals(listFormatAlignment, that.listFormatAlignment)
        && Objects.equals(listFormatNumberFormat, that.listFormatNumberFormat)
        && Objects.equals(listFormatNumberStyle, that.listFormatNumberStyle)
        && Objects.equals(id, that.id) && Objects.equals(content, that.content)
        && Objects.equals(specialSymbolsCount, that.specialSymbolsCount)
        && Objects.equals(wordsCount, that.wordsCount)
        && Objects.equals(symbolCount, that.symbolCount) && Objects.equals(lowercase, that.lowercase)
        && Objects.equals(uppercase, that.uppercase)
        && Objects.equals(lastSymbolPd, that.lastSymbolPd)
        && Objects.equals(firstKeyWord, that.firstKeyWord)
        && Objects.equals(prevElementMark, that.prevElementMark)
        && Objects.equals(curElementMark, that.curElementMark)
        && Objects.equals(nextElementMark, that.nextElementMark)
        && Objects.equals(fullBold, that.fullBold) && Objects.equals(fullItalic, that.fullItalic)
        && Objects.equals(alignment, that.alignment)
        && Objects.equals(keepLinesTogether, that.keepLinesTogether)
        && Objects.equals(keepWithNext, that.keepWithNext)
        && Objects.equals(leftIndentation, that.leftIndentation)
        && Objects.equals(lineSpacing, that.lineSpacing)
        && Objects.equals(noSpaceBetweenParagraphsOfSameStyle, that.noSpaceBetweenParagraphsOfSameStyle)
        && Objects.equals(outlineLevel, that.outlineLevel)
        && Objects.equals(pageBreakBefore, that.pageBreakBefore)
        && Objects.equals(rightIndentation, that.rightIndentation)
        && Objects.equals(spaceAfter, that.spaceAfter)
        && Objects.equals(spaceBefore, that.spaceBefore)
        && Objects.equals(specialIndentation, that.specialIndentation);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(listFormatIsList, listFormatLevel, listFormatAlignment, listFormatNumberFormat, listFormatNumberStyle,
            specialSymbolsCount, wordsCount, symbolCount, lowercase, uppercase, lastSymbolPd, fullBold, fullItalic,
            alignment, keepLinesTogether, keepWithNext, leftIndentation, lineSpacing,
            noSpaceBetweenParagraphsOfSameStyle,
            outlineLevel, pageBreakBefore, rightIndentation, spaceAfter, spaceBefore, specialIndentation);
  }
}
