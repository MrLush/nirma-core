package ru.domozavr.core.domain.entity;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "classification_cases")
public class ClassificationCase {
  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  private UUID id;
  private boolean classifiedSuccessfully;
  private Long paragraphId;
  private UUID documentId;
  private UUID userId;
  private String classifiedClass;
  private String classChosenByUser;
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdAt;
  private String specialSymbolsCount;
  private String wordsCount;
  private String symbolCount;
  private String lowercase;
  private String uppercase;
  private String lastSymbolPd;
  private String firstKeyWord;
  private String prevElementMark;
  private String nextElementMark;
  private String fullBold;
  private String fullItalic;
  private String alignment;
  private String keepLinesTogether;
  private String keepWithNext;
  private String leftIndentation;
  private String lineSpacing;
  private String noSpaceBetweenParagraphsOfSameStyle;
  private String outlineLevel;
  private String pageBreakBefore;
  private String rightIndentation;
  private String spaceAfter;
  private String spaceBefore;
  private String specialIndentation;
}
