package ru.domozavr.core.domain;

import com.opencsv.bean.CsvBindByPosition;
import java.io.UnsupportedEncodingException;

public class OrderLine {

  @CsvBindByPosition(position = 0)
  private String specId;
  @CsvBindByPosition(position = 1)
  private String id;
  @CsvBindByPosition(position = 2)
  private String specialSymbolsCount;
  @CsvBindByPosition(position = 3)
  private String wordsCount;
  @CsvBindByPosition(position = 4)
  private String symbolCount;
  @CsvBindByPosition(position = 5)
  private String lowercase;
  @CsvBindByPosition(position = 6)
  private String uppercase;
  @CsvBindByPosition(position = 7)
  private String lastSymbolPd;
  @CsvBindByPosition(position = 8)
  private String firstKeyWord;
  @CsvBindByPosition(position = 9)
  private String prevElementMark;
  @CsvBindByPosition(position = 10)
  private String curElementMark;
  @CsvBindByPosition(position = 11)
  private String nextElementMark;
  @CsvBindByPosition(position = 12)
  private String fullBold;
  @CsvBindByPosition(position = 13)
  private String fullItalic;
  @CsvBindByPosition(position = 14)
  private String alignment;
  @CsvBindByPosition(position = 15)
  private String keepLinesTogether;
  @CsvBindByPosition(position = 16)
  private String keepWithNext;
  @CsvBindByPosition(position = 17)
  private String leftIndentation;
  @CsvBindByPosition(position = 18)
  private String lineSpacing;
  @CsvBindByPosition(position = 19)
  private String noSpaceBetweenParagraphsOfSameStyle;
  @CsvBindByPosition(position = 20)
  private String outlineLevel;
  @CsvBindByPosition(position = 21)
  private String pageBreakBefore;
  @CsvBindByPosition(position = 22)
  private String rightIndentation;
  @CsvBindByPosition(position = 23)
  private String spaceAfter;
  @CsvBindByPosition(position = 24)
  private String spaceBefore;
  @CsvBindByPosition(position = 25)
  private String specialIndentation;
  @CsvBindByPosition(position = 26)
  private String predict;

  public String getSpecId() {
    return specId;
  }

  public void setSpecId(String specId) {
    this.specId = specId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSpecialSymbolsCount() {
    return specialSymbolsCount;
  }

  public void setSpecialSymbolsCount(String specialSymbolsCount) {
    this.specialSymbolsCount = specialSymbolsCount;
  }

  public String getWordsCount() {
    return wordsCount;
  }

  public void setWordsCount(String wordsCount) {
    this.wordsCount = wordsCount;
  }

  public String getSymbolCount() {
    return symbolCount;
  }

  public void setSymbolCount(String symbolCount) {
    this.symbolCount = symbolCount;
  }

  public String getLowercase() {
    return lowercase;
  }

  public void setLowercase(String lowercase) {
    this.lowercase = lowercase;
  }

  public String getUppercase() {
    return uppercase;
  }

  public void setUppercase(String uppercase) {
    this.uppercase = uppercase;
  }

  public String getLastSymbolPd() {
    return lastSymbolPd;
  }

  public void setLastSymbolPd(String lastSymbolPd) {
    this.lastSymbolPd = lastSymbolPd;
  }

  public String getFirstKeyWord() {
    return firstKeyWord;
  }

  /**
   * We have this construction because of russian language in this field.
   *
   * @param firstKeyWord - field which contains first paragraph word
   */
  public void setFirstKeyWord(String firstKeyWord) {
    try {
      this.firstKeyWord = new String(firstKeyWord.getBytes(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      this.firstKeyWord = firstKeyWord;
    }
  }

  public String getPrevElementMark() {
    return prevElementMark;
  }

  public void setPrevElementMark(String prevElementMark) {
    this.prevElementMark = prevElementMark;
  }

  public String getCurElementMark() {
    return curElementMark;
  }

  public void setCurElementMark(String curElementMark) {
    this.curElementMark = curElementMark;
  }

  public String getNextElementMark() {
    return nextElementMark;
  }

  public void setNextElementMark(String nextElementMark) {
    this.nextElementMark = nextElementMark;
  }

  public String getFullBold() {
    return fullBold;
  }

  public void setFullBold(String fullBold) {
    this.fullBold = fullBold;
  }

  public String getFullItalic() {
    return fullItalic;
  }

  public void setFullItalic(String fullItalic) {
    this.fullItalic = fullItalic;
  }

  public String getAlignment() {
    return alignment;
  }

  public void setAlignment(String alignment) {
    this.alignment = alignment;
  }

  public String getKeepLinesTogether() {
    return keepLinesTogether;
  }

  public void setKeepLinesTogether(String keepLinesTogether) {
    this.keepLinesTogether = keepLinesTogether;
  }

  public String getKeepWithNext() {
    return keepWithNext;
  }

  public void setKeepWithNext(String keepWithNext) {
    this.keepWithNext = keepWithNext;
  }

  public String getLeftIndentation() {
    return leftIndentation;
  }

  public void setLeftIndentation(String leftIndentation) {
    this.leftIndentation = leftIndentation;
  }

  public String getLineSpacing() {
    return lineSpacing;
  }

  public void setLineSpacing(String lineSpacing) {
    this.lineSpacing = lineSpacing;
  }

  public String getNoSpaceBetweenParagraphsOfSameStyle() {
    return noSpaceBetweenParagraphsOfSameStyle;
  }

  public void setNoSpaceBetweenParagraphsOfSameStyle(String noSpaceBetweenParagraphsOfSameStyle) {
    this.noSpaceBetweenParagraphsOfSameStyle = noSpaceBetweenParagraphsOfSameStyle;
  }

  public String getOutlineLevel() {
    return outlineLevel;
  }

  public void setOutlineLevel(String outlineLevel) {
    this.outlineLevel = outlineLevel;
  }

  public String getPageBreakBefore() {
    return pageBreakBefore;
  }

  public void setPageBreakBefore(String pageBreakBefore) {
    this.pageBreakBefore = pageBreakBefore;
  }

  public String getRightIndentation() {
    return rightIndentation;
  }

  public void setRightIndentation(String rightIndentation) {
    this.rightIndentation = rightIndentation;
  }

  public String getSpaceAfter() {
    return spaceAfter;
  }

  public void setSpaceAfter(String spaceAfter) {
    this.spaceAfter = spaceAfter;
  }

  public String getSpaceBefore() {
    return spaceBefore;
  }

  public void setSpaceBefore(String spaceBefore) {
    this.spaceBefore = spaceBefore;
  }

  public String getSpecialIndentation() {
    return specialIndentation;
  }

  public void setSpecialIndentation(String specialIndentation) {
    this.specialIndentation = specialIndentation;
  }

  public String getPredict() {
    return predict;
  }

  public void setPredict(String predict) {
    this.predict = predict;
  }
}
