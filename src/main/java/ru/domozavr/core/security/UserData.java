package ru.domozavr.core.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserData {
  private UUID sub;
  private String email;
  private String firstName;
  private String lastName;
  private Set<String> roles;
  private boolean temporary;
  private Instant exp;
}
