package ru.domozavr.core.security;

import java.util.Collections;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;

// Here we can add additional roles and sub-roles for our users
public enum Role implements GrantedAuthority {
  USER,
  ADMIN;

  public static final List<Role> userRoles = Collections.singletonList(USER);

  public static final List<Role> adminRoles = Collections.singletonList(ADMIN);

  @Override
  public String getAuthority() {
    return name();
  }
}
