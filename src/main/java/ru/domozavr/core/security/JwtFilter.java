package ru.domozavr.core.security;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.domain.entity.UserEntity;
import ru.domozavr.core.exception.SecurityException;
import ru.domozavr.core.repository.UserRepository;

@RequiredArgsConstructor
@Component
@Slf4j
public class JwtFilter extends GenericFilterBean {

  private final JwtProvider jwtProvider;
  private final UserRepository userRepository;

  @Override
  public void doFilter(ServletRequest request, ServletResponse response,
                       FilterChain chain) throws IOException, ServletException {
    log.info("Filtering...");
    String token = jwtProvider.getTokenFromRequest((HttpServletRequest) request).orElse("undefined");
    if (!token.equals("undefined") && jwtProvider.validateToken(token)) {
      log.info("Token is valid and not null.");
      String email = jwtProvider.getSubFromToken(token);
      UserEntity userEntity = userRepository.findByEmailIgnoreCase(email)
          .orElseThrow(() -> new SecurityException(ServiceError.NO_SUCH_ENTITY,
              "Entity with email " + email + " does not exist."));
      if (userEntity.getLastToken().equals(token)) {
        log.info("Token is equal to last user token.");
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
            new AuthorisedUser(userEntity.getId(), userEntity.getEmail(), userEntity.getRoles()),
            null, userEntity.getRoles());
        SecurityContextHolder.getContext().setAuthentication(auth);
      } else {
        log.info("Token is not equal to last user token.");
      }
    }
    chain.doFilter(request, response);
  }
}
