package ru.domozavr.core.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "paths")
public class PathsProperties {
  private String corePath;
  private String documentsFileBase;
  private String docxCorrectorPath;
}
