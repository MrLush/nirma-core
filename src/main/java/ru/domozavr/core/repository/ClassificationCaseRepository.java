package ru.domozavr.core.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.domozavr.core.domain.entity.ClassificationCase;

@Repository
public interface ClassificationCaseRepository extends JpaRepository<ClassificationCase, UUID> {

}
