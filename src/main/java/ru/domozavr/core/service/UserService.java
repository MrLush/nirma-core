package ru.domozavr.core.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.domozavr.core.domain.dto.CreateUserDto;
import ru.domozavr.core.domain.dto.UpdateUserRoleDto;
import ru.domozavr.core.domain.dto.UserDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.domain.entity.UserEntity;
import ru.domozavr.core.exception.NotFoundException;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.mapper.UserMapper;
import ru.domozavr.core.repository.UserRepository;
import ru.domozavr.core.security.Role;

@Service
@RequiredArgsConstructor
public class UserService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final UserMapper userMapper;

  @Transactional(readOnly = true)
  public UserDto getById(UUID id) {
    return userRepository.findById(id)
        .map(userMapper::convert)
        .orElseThrow(() -> new NotFoundException(ServiceError.NO_SUCH_ENTITY,
            "Entity with id " + id + " does not exist."));
  }

  @Transactional(readOnly = true)
  public Page<UserDto> getAll(Pageable pageable) {
    return userRepository.findAll(pageable).map(userMapper::convert);
  }

  @Transactional
  public UserDto createUser(CreateUserDto createUserDto) {
    UserEntity newUser = userMapper.convert(createUserDto);
    newUser.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
    newUser.setRoles(Role.userRoles);
    newUser.setLastToken("undefined");
    return userMapper.convert(userRepository.save(newUser));
  }

  @Transactional
  public UserDto updateUserRoles(UpdateUserRoleDto updateUserRoleDto) {
    UUID userId = updateUserRoleDto.getId();
    UserEntity storedUser = userRepository.findById(userId)
        .orElseThrow(
            () -> new NotFoundException(ServiceError.NO_SUCH_ENTITY,
                "Entity with id " + userId + " does not exist."));
    storedUser.setRoles(updateUserRoleDto.getRoles());
    UserEntity savedEntity = userRepository.save(storedUser);
    return userMapper.convert(savedEntity);
  }

  @Transactional(readOnly = true)
  public boolean isExistsByEmail(String email) {
    return userRepository.existsByEmailIgnoreCase(email);
  }

  @Transactional(readOnly = true)
  public boolean isExistsByEmailAndPassword(String email, String password) {
    UserEntity userEntity = userRepository.findByEmailIgnoreCase(email)
        .orElseThrow(
            () -> new ValidationException(ServiceError.WRONG_CREDENTIALS, "Email or password is incorrect."));
    return passwordEncoder.matches(password, userEntity.getPassword());
  }

  @Transactional
  public void invalidateUserToken(String email) {
    UserEntity userEntity = userRepository.findByEmailIgnoreCase(email)
        .orElseThrow(
            () -> new NotFoundException(ServiceError.NO_SUCH_ENTITY,
                "Entity with email " + email + " does not exist."));
    userEntity.setLastToken("undefined");
    userRepository.save(userEntity);
  }
}
