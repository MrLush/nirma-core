package ru.domozavr.core.service;

import static ru.domozavr.core.domain.dto.error.ServiceError.LINE_NOT_FOUND;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ru.domozavr.core.domain.PropertiesWithPresets;
import ru.domozavr.core.domain.dto.ClassificationCaseDto;
import ru.domozavr.core.domain.entity.ClassificationCase;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.mapper.ClassificationCaseMapper;
import ru.domozavr.core.repository.ClassificationCaseRepository;
import ru.domozavr.core.util.FileUtil;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClassificationCaseService {
  private static final String FILE_NAME = "/result3.csv";

  private final ClassificationCaseRepository repository;
  private final AuthenticationService authenticationService;
  private final FileUtil fileUtil;
  private final ClassificationCaseMapper classificationCaseMapper;

  public void addClassificationCase(ClassificationCaseDto caseDto) {
    UUID userId = authenticationService.getAuthenticatedUser().getId();
    String filePath = fileUtil.getDocumentPath(caseDto.getDocumentId(), userId) + FILE_NAME;
    List<PropertiesWithPresets> predicates = fileUtil.getPojoFromCsv(PropertiesWithPresets.class, filePath);
    PropertiesWithPresets predicatesFromCase = predicates.stream()
        .filter(line -> line.getId().equals(caseDto.getParagraphId()))
        .findFirst()
        .orElseThrow(() -> new ValidationException(LINE_NOT_FOUND,
            "CSV line with paragraph id " + caseDto.getParagraphId() + " not found"));
    ClassificationCase classificationCase = classificationCaseMapper.convert(caseDto, predicatesFromCase);
    try {
      repository.save(classificationCase);
    } catch (DataIntegrityViolationException e) {
      log.info("Exception because of inserting duplicate of predicates: " + e.getMessage());
    }
  }
}
