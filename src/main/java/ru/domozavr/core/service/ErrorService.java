package ru.domozavr.core.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.domozavr.core.configuration.properties.PathsProperties;
import ru.domozavr.core.domain.dto.ErrorsDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ServerException;
import ru.domozavr.core.util.FileUtil;
import ru.domozavr.core.util.TerminalUtil;

@Slf4j
@Service
@RequiredArgsConstructor
public class ErrorService {
  private static final String DOCX_CORRECTOR_COMMAND_CORRECT = "correct";
  private static final String DOCX_CORRECTOR_COMMAND_CORRECT_PARAGRAPH = "correctParagraph";
  private static final String ERROR_RESULT_FILE_NAME = "mistakes.json";

  private final AuthenticationService authenticationService;
  private final TerminalUtil terminalUtil;
  private final PathsProperties pathsProperties;
  private final FileUtil fileUtil;

  public String getLineErrors(ErrorsDto errorsDto) {
    UUID userId = authenticationService.getAuthenticatedUser().getId();
    String basePath = fileUtil.getDocumentPath(UUID.fromString(errorsDto.getDocumentId()), userId) + "/";
    String documentPath =
        fileUtil.getDocumentPathWithFilename(UUID.fromString(errorsDto.getDocumentId()), userId).toString();
    String requirements = fileUtil.getRequirementsNameByStandard(errorsDto.getStandard());
    String commandForDocxCorrector = getDocxCorrectorCommand(DOCX_CORRECTOR_COMMAND_CORRECT_PARAGRAPH,
        documentPath, requirements, errorsDto.getParagraphId(), errorsDto.getClassifiedClass(), basePath);
    terminalUtil.executeCommand(commandForDocxCorrector);
    String resultPath = basePath + ERROR_RESULT_FILE_NAME;
    return parseStringFromFile(resultPath);
  }

  public String getDocumentErrors(ErrorsDto errorsDto) {
    UUID userId = authenticationService.getAuthenticatedUser().getId();
    String basePath = fileUtil.getDocumentPath(UUID.fromString(errorsDto.getDocumentId()), userId) + "/";
    //TODO Add here correct path to file with document paragraphsIds and Classes
    String classificationResult = basePath + "classificationResult.json";
    String documentPath =
        fileUtil.getDocumentPathWithFilename(UUID.fromString(errorsDto.getDocumentId()), userId).toString();
    String requirements = fileUtil.getRequirementsNameByStandard(errorsDto.getStandard());
    String commandForDocxCorrector = getDocxCorrectorCommand(DOCX_CORRECTOR_COMMAND_CORRECT,
        documentPath, requirements, classificationResult, basePath);
    terminalUtil.executeCommand(commandForDocxCorrector);
    //TODO Check that correct command returns this file
    String resultPath = basePath + ERROR_RESULT_FILE_NAME;
    return parseStringFromFile(resultPath);
  }

  private String getDocxCorrectorCommand(String command, String documentPath, String param3,
                                         String param4, String param5, String param6) {
    String docxCorrectorPath = pathsProperties.getDocxCorrectorPath();
    return docxCorrectorPath + " "
        + command + " "
        + documentPath + " "
        + param3 + " "
        + param4 + " "
        + param5 + " "
        + param6;
  }

  private String getDocxCorrectorCommand(String command, String documentPath, String param3,
                                         String param4, String param5) {
    String docxCorrectorPath = pathsProperties.getDocxCorrectorPath();
    return docxCorrectorPath + " "
        + command + " "
        + documentPath + " "
        + param3 + " "
        + param4 + " "
        + param5;
  }

  private String parseStringFromFile(String path) {
    String result;
    try {
      // in Java 11 we can change this part to code below
      // result = Files.readString(Paths.get(path), StandardCharsets.UTF_8);
      Stream<String> lines = Files.lines(Paths.get(path));
      String data = lines.collect(Collectors.joining("\n"));
      lines.close();
      result = data.trim();
    } catch (IOException e) {
      log.error(e.getMessage());
      throw new ServerException(ServiceError.PARSE_ERROR, "Can not parse result file");
    }
    return result;
  }
}
