package ru.domozavr.core.service;

import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.domozavr.core.domain.dto.CreateUserDto;
import ru.domozavr.core.domain.dto.LoginDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.security.JwtProvider;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {

  private final UserService userService;
  private final JwtProvider jwtProvider;

  public String register(CreateUserDto user) {
    if (!userService.isExistsByEmail(user.getEmail())) {
      userService.createUser(user);
      return jwtProvider.generateToken(user.getEmail());
    } else {
      throw new ValidationException(ServiceError.REQUEST_EXIST, "User with these credentials already exists: "
          + user.getEmail());
    }
  }

  public String login(LoginDto loginDto) {
    if (userService.isExistsByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword())) {
      return jwtProvider.generateToken(loginDto.getEmail());
    } else {
      throw new ValidationException(ServiceError.WRONG_CREDENTIALS, "Username or password is incorrect.");
    }
  }

  public void logout(HttpServletRequest request) {
    log.info("Logout request");
    String token = jwtProvider.getTokenFromRequest(request).orElse("undefined");
    if (!token.equals("undefined")) {
      log.info("Invalidating token...");
      String email = jwtProvider.getSubFromToken(token);
      userService.invalidateUserToken(email);
    } else {
      throw new ValidationException(ServiceError.EMPTY_TOKEN_IN_REQUEST, "No token in request");
    }
  }
}
