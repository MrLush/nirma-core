package ru.domozavr.core.service;

import com.google.common.hash.Hashing;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.domozavr.core.configuration.properties.PathsProperties;
import ru.domozavr.core.domain.PropertiesWithPresets;
import ru.domozavr.core.domain.dto.ClassifiedLineDto;
import ru.domozavr.core.domain.dto.DocumentDto;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ServerException;
import ru.domozavr.core.exception.ValidationException;
import ru.domozavr.core.util.FileUtil;
import ru.domozavr.core.util.TerminalUtil;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentService {
  private static final String DOCX_CORRECTOR_COMMAND_PULL = "pull";
  private static final String DOCX_CORRECTOR_COMMAND_PULL_WITH_PRESETS = "pullWithPresets";
  private static final String RESULT_PATH_1 = "result1.csv";
  private static final String RESULT_PATH_2 = "result2.csv";
  private static final String RESULT_PATH_3 = "result3.csv";
  private static final String EMPTY_FIELD = "";

  private final PathsProperties pathsProperties;
  private final TerminalUtil terminalUtil;
  private final FileUtil fileUtil;
  private final AuthenticationService authenticationService;

  public DocumentDto saveFile(MultipartFile file, String standard, String startFromParagraph) {
    long start = System.nanoTime();
    UUID documentId = UUID.randomUUID(); //unique filename
    UUID userId = authenticationService.getAuthenticatedUser().getId();
    String documentPath = copyFile(file, userId, documentId);
    String fullFilePath = getFilePathWithoutFileName(documentPath);

    double elapsedTimeInSec = (System.nanoTime() - start) * 1.0e-9;
    log.info("Time of loading file on server: " + elapsedTimeInSec);
    start = System.nanoTime();

    String resultPath1 = fullFilePath + RESULT_PATH_1;
    String resultPath2 = fullFilePath + RESULT_PATH_2;
    String resultPath3 = fullFilePath + RESULT_PATH_3;
    String presetsFile = fileUtil.getPresetsFilePathByStandard(standard);

    String firstCommandForDocxCorrector = getDocxCorrectorCommand(DOCX_CORRECTOR_COMMAND_PULL, documentPath,
        startFromParagraph, resultPath1, resultPath2);
    String secondCommandForDocxCorrector = getDocxCorrectorCommand(DOCX_CORRECTOR_COMMAND_PULL_WITH_PRESETS,
        documentPath, presetsFile, startFromParagraph, resultPath3);
    terminalUtil.executeCommand(firstCommandForDocxCorrector);
    terminalUtil.executeCommand(secondCommandForDocxCorrector);

    elapsedTimeInSec = (System.nanoTime() - start) * 1.0e-9;
    log.info("Time of DocxCorrector work: " + elapsedTimeInSec);

    documentPath = documentPath.replace("\\", "/");
    start = System.nanoTime();
    List<PropertiesWithPresets> predicates = fileUtil.getPojoFromCsv(PropertiesWithPresets.class, resultPath3);
    List<ClassifiedLineDto> classifiedLineDtoList = new ArrayList<>();
    predicates.forEach(line -> {
      if (line.getId() != null) {
        classifiedLineDtoList.add(new ClassifiedLineDto()
            .setParagraphId(line.getId())
            .setStandardWordPredict(line.getClassesFromWord())
            .setGostsPredict(line.getClassesFromRequirements())
            .setCurElementMark(line.getCurElementMark())
            .setPredicatesGroup(Hashing.sha256()
                .hashString(String.valueOf(line.hashCode()), StandardCharsets.UTF_8)
                .toString())
            .setChecked(false)
            .setFinalClass(EMPTY_FIELD));
      }
    });
    elapsedTimeInSec = (System.nanoTime() - start) * 1.0e-9;
    log.info("Time of mapping result3 for response: " + elapsedTimeInSec);
    return new DocumentDto()
        .setId(documentId)
        .setDocumentPath(documentPath)
        .setPreClassifiedDocument(classifiedLineDtoList);
  }

  private String getDocxCorrectorCommand(String command, String documentPath, String param3,
                                         String param4, String param5) {
    String docxCorrectorPath = pathsProperties.getDocxCorrectorPath();
    return docxCorrectorPath + " "
        + command + " "
        + documentPath + " "
        + param3 + " "
        + param4 + " "
        + param5;
  }

  private String copyFile(MultipartFile file, UUID userId, UUID documentId) {
    String documentPath;
    checkThatItsDocx(file);
    try (InputStream is = file.getInputStream()) {
      Path path = Paths.get(fileUtil.getDocumentPathWithFilename(documentId, userId).toString());
      documentPath = path.toString();
      Files.copy(is, path);
    } catch (IOException ie) {
      log.warn(ie.toString());
      throw new ServerException(ServiceError.GENERIC_ERROR, "Failed to upload file");
    }
    return documentPath;
  }

  private void checkThatItsDocx(MultipartFile file) {
    String fileExtension = getFileExtension(file);
    if (!fileExtension.equals(".docx")) {
      throw new ValidationException(ServiceError.WRONG_FILE_EXTENSION, "File " + file.getOriginalFilename()
          + " contains not supported extension: " + fileExtension);
    }
  }

  private String getFileExtension(MultipartFile file) {
    String name = file.getOriginalFilename();
    assert name != null;
    int lastIndexOf = name.lastIndexOf(".");
    if (lastIndexOf == -1) {
      return ""; // empty extension
    }
    return name.substring(lastIndexOf);
  }

  private String getFilePathWithoutFileName(String path) {
    String filePath;
    int lastIndexOf = path.lastIndexOf("\\");
    if (lastIndexOf == -1) {
      return ""; // empty extension
    }
    filePath = path.substring(0, lastIndexOf + 1);
    return filePath;
  }
}
