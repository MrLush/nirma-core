package ru.domozavr.core.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = PasswordValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Password {

  String message() default "must be more than 8 characters long and contains at least one letter in lowercase, "
      + "one letter in uppercase and one digit";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
