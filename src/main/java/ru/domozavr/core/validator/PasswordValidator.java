package ru.domozavr.core.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {

  @Override
  public boolean isValid(String password, ConstraintValidatorContext context) {
    return password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$") && password.length() >= 8;
  }
}