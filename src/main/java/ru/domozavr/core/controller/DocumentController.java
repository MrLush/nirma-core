package ru.domozavr.core.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.domozavr.core.domain.dto.DocumentDto;
import ru.domozavr.core.service.DocumentService;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DocumentController {
  private final DocumentService documentService;

  @PostMapping("/documents")
  public ResponseEntity<DocumentDto> saveFile(@RequestParam("file") MultipartFile file,
                                              @RequestParam("standard") String standard,
                                              @RequestParam("start-from-paragraph") String startFromParagraph) {
    return ResponseEntity.ok(documentService.saveFile(file, standard, startFromParagraph));
  }
}
