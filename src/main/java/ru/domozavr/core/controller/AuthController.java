package ru.domozavr.core.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.domozavr.core.domain.dto.JwtDto;
import ru.domozavr.core.domain.dto.LoginDto;
import ru.domozavr.core.service.AuthService;

@RestController
@RequiredArgsConstructor
public class AuthController {

  private final AuthService authService;

  @PreAuthorize("permitAll()")
  @PostMapping("/session")
  public ResponseEntity<JwtDto> login(@RequestBody @Valid LoginDto loginDto) {
    return ResponseEntity.ok(new JwtDto(authService.login(loginDto)));
  }

  @PreAuthorize("isAuthenticated()")
  @DeleteMapping("/session")
  public void logout(HttpServletRequest request) {
    authService.logout(request);
  }
}
