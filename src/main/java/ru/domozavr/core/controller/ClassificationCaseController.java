package ru.domozavr.core.controller;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.domozavr.core.domain.dto.ClassificationCaseDto;
import ru.domozavr.core.service.ClassificationCaseService;

@RestController
@RequiredArgsConstructor
public class ClassificationCaseController {
  private final ClassificationCaseService classificationCaseService;

  @PreAuthorize("isAuthenticated()")
  @PostMapping("/classificationCase")
  public void addClassificationCase(@RequestBody @Valid ClassificationCaseDto classificationCaseDto) {
    classificationCaseService.addClassificationCase(classificationCaseDto);
  }
}
