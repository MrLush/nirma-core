package ru.domozavr.core.controller;

import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.domozavr.core.domain.dto.CreateUserDto;
import ru.domozavr.core.domain.dto.JwtDto;
import ru.domozavr.core.domain.dto.UpdateUserRoleDto;
import ru.domozavr.core.domain.dto.UserDto;
import ru.domozavr.core.service.AuthService;
import ru.domozavr.core.service.UserService;

@RestController
@RequiredArgsConstructor
public class UserController {
  private final UserService userService;
  private final AuthService authService;

  @PreAuthorize("permitAll()")
  @PostMapping("/users")
  public ResponseEntity<JwtDto> register(@RequestBody @Valid CreateUserDto createUserDto) {
    return ResponseEntity.ok(new JwtDto(authService.register(createUserDto)));
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @GetMapping("/users/{userId}")
  public ResponseEntity<UserDto> getById(@PathVariable("userId") UUID userId) {
    return ResponseEntity.ok(userService.getById(userId));
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @GetMapping("/users")
  public ResponseEntity<Page<UserDto>> allUsers(
      @PageableDefault(sort = "username", direction = Sort.Direction.ASC, size = 5)
          Pageable pageable) {
    return ResponseEntity.ok(userService.getAll(pageable));
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @PutMapping("/users")
  public ResponseEntity<UserDto> update(@Valid @RequestBody UpdateUserRoleDto updateUserRoleDto) {
    return ResponseEntity.ok(userService.updateUserRoles(updateUserRoleDto));
  }
}
