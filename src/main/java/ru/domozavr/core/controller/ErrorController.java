package ru.domozavr.core.controller;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.domozavr.core.domain.dto.ErrorsDto;
import ru.domozavr.core.service.ErrorService;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ErrorController {
  private final ErrorService errorService;

  @PreAuthorize("isAuthenticated()")
  @GetMapping(value = "/lineErrors", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> getLineErrors(@RequestBody @Valid ErrorsDto errorsDto) {
    return ResponseEntity.ok(errorService.getLineErrors(errorsDto));
  }

  @PreAuthorize("isAuthenticated()")
  @GetMapping(value = "/documentErrors", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> getDocumentErrors(@RequestBody @Valid ErrorsDto errorsDto) {
    return ResponseEntity.ok(errorService.getDocumentErrors(errorsDto));
  }
}
