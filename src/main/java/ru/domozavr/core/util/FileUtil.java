package ru.domozavr.core.util;

import static ru.domozavr.core.domain.dto.error.ServiceError.FILE_NOT_FOUND;

import com.opencsv.bean.CsvToBeanBuilder;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.domozavr.core.configuration.properties.PathsProperties;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ServerException;
import ru.domozavr.core.exception.ValidationException;

@Component
@RequiredArgsConstructor
public class FileUtil {
  private static final String GOST_7_32 = "GOST_7_32";
  private static final String GOST_7_32_REQ = "GOST";
  private static final String GOST_7_0_11 = "GOST_7_0_11";
  private static final String GOST_7_32_PRESETS_FILE_NAME = "modelGOST.json";
  private static final String GOST_7_0_11_PRESETS_FILE_NAME = "modelGOST_7_0_11.json";

  private final PathsProperties pathsProperties;

  public void createDirectoryIfItDoesNotExist(String dir) {
    final Path path = Paths.get(dir);
    if (Files.notExists(path)) {
      try {
        Files.createDirectories(path);
      } catch (IOException ie) {
        throw new ServerException(ServiceError.GENERIC_ERROR,
            "Server can not create new directory with this path: " + path);
      }
    }
  }

  public StringBuilder getDocumentPath(UUID documentId, UUID userId) {
    StringBuilder sb = new StringBuilder()
        .append(pathsProperties.getDocumentsFileBase())
        .append("/")
        .append("user-")
        .append(userId.toString())  // Appending uuid to create folder
        .append("/")
        .append("document-")
        .append(documentId.toString());          // Appending uuid to create folder
    createDirectoryIfItDoesNotExist(sb.toString());
    return sb;
  }

  public StringBuilder getDocumentPathWithFilename(UUID documentId, UUID userId) {
    return getDocumentPath(documentId, userId)
        .append("/")
        .append(documentId)
        .append(".docx");
  }

  public <P> List<P> getPojoFromCsv(Class<P> pojoTypeParameterClass, String csvFileName) {
    List<P> result;
    try {
      result = (List<P>) new CsvToBeanBuilder(new FileReader(csvFileName))
          .withType(pojoTypeParameterClass)
          .withSeparator(';')
          .withSkipLines(1)
          .build()
          .parse();
    } catch (FileNotFoundException e) {
      throw new ValidationException(FILE_NOT_FOUND, "file " + csvFileName + " not found");
    }
    return result;
  }

  public String getPresetsFilePathByStandard(String standard) {
    switch (standard) {
      case (GOST_7_32):
        return pathsProperties.getCorePath() + GOST_7_32_PRESETS_FILE_NAME;
      case (GOST_7_0_11):
        return pathsProperties.getCorePath() + GOST_7_0_11_PRESETS_FILE_NAME;
      default:
        return pathsProperties.getCorePath() + GOST_7_32_PRESETS_FILE_NAME;
    }
  }

  public String getRequirementsNameByStandard(String standard) {
    switch (standard) {
      case (GOST_7_32):
        return GOST_7_32_REQ;
      case (GOST_7_0_11):
        return GOST_7_0_11;
      default:
        return GOST_7_32_REQ;
    }
  }
}