package ru.domozavr.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import ru.domozavr.core.domain.dto.error.ServiceError;
import ru.domozavr.core.exception.ServerException;

@Slf4j
@Controller
public class TerminalUtil {
  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss:SSS");

  public void executeCommand(String command) {
    try {
      log(command);
      Process process = Runtime.getRuntime().exec(command);
      logOutput(process.getInputStream(), "");
      logOutput(process.getErrorStream(), "Error: ");
      process.waitFor();
    } catch (IOException | InterruptedException e) {
      log.error(e.getMessage());
      throw new ServerException(ServiceError.GENERIC_ERROR, "Error occurs when terminal program executed");
    }
  }

  private void logOutput(InputStream inputStream, String prefix) {
    new Thread(() -> {
      Scanner scanner = new Scanner(inputStream, "UTF-8");
      while (scanner.hasNextLine()) {
        synchronized (this) {
          log(prefix + scanner.nextLine());
        }
      }
      scanner.close();
    }).start();
  }

  private synchronized void log(String message) {
    log.debug(DATE_FORMAT.format(new Date()) + ": " + message);
  }
}
