package ru.domozavr.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.domozavr.core.domain.dto.CreateUserDto;
import ru.domozavr.core.domain.dto.UserDto;
import ru.domozavr.core.domain.entity.UserEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

  UserDto convert(UserEntity userEntity);

  UserEntity convert(CreateUserDto createUserDto);
}
