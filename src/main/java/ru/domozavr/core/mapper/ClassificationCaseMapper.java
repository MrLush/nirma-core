package ru.domozavr.core.mapper;

import java.util.Date;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.domozavr.core.domain.PropertiesWithPresets;
import ru.domozavr.core.domain.dto.ClassificationCaseDto;
import ru.domozavr.core.domain.entity.ClassificationCase;
import ru.domozavr.core.service.AuthenticationService;

@Component
@RequiredArgsConstructor
public class ClassificationCaseMapper {
  private final AuthenticationService authenticationService;

  public ClassificationCase convert(ClassificationCaseDto classificationCaseDto, PropertiesWithPresets predictors) {
    if (classificationCaseDto == null) {
      return null;
    }
    UUID userId = authenticationService.getAuthenticatedUser().getId();
    ClassificationCase classificationCase = new ClassificationCase();
    classificationCase.setClassifiedSuccessfully(classificationCaseDto.isClassifiedSuccessfully());
    classificationCase.setParagraphId(Long.parseLong(classificationCaseDto.getParagraphId()));
    classificationCase.setDocumentId(classificationCaseDto.getDocumentId());
    classificationCase.setUserId(userId);
    classificationCase.setClassifiedClass(classificationCaseDto.getClassifiedClass());
    classificationCase.setClassChosenByUser(classificationCaseDto.getClassChosenByUser());
    classificationCase.setCreatedAt(new Date(System.currentTimeMillis()));
    //predictors
    classificationCase.setSpecialSymbolsCount(predictors.getSpecialSymbolsCount());
    classificationCase.setWordsCount(predictors.getWordsCount());
    classificationCase.setSymbolCount(predictors.getSymbolCount());
    classificationCase.setLowercase(predictors.getLowercase());
    classificationCase.setUppercase(predictors.getUppercase());
    classificationCase.setLastSymbolPd(predictors.getLastSymbolPd());
    classificationCase.setFirstKeyWord(predictors.getFirstKeyWord());
    classificationCase.setPrevElementMark(predictors.getPrevElementMark());
    classificationCase.setNextElementMark(predictors.getNextElementMark());
    classificationCase.setFullBold(predictors.getFullBold());
    classificationCase.setFullItalic(predictors.getFullItalic());
    classificationCase.setAlignment(predictors.getAlignment());
    classificationCase.setKeepLinesTogether(predictors.getKeepLinesTogether());
    classificationCase.setKeepWithNext(predictors.getKeepWithNext());
    classificationCase.setLeftIndentation(predictors.getLeftIndentation());
    classificationCase.setLineSpacing(predictors.getLineSpacing());
    classificationCase.setNoSpaceBetweenParagraphsOfSameStyle(predictors.getNoSpaceBetweenParagraphsOfSameStyle());
    classificationCase.setOutlineLevel(predictors.getOutlineLevel());
    classificationCase.setPageBreakBefore(predictors.getPageBreakBefore());
    classificationCase.setRightIndentation(predictors.getRightIndentation());
    classificationCase.setSpaceAfter(predictors.getSpaceAfter());
    classificationCase.setSpaceBefore(predictors.getSpaceBefore());
    classificationCase.setSpecialIndentation(predictors.getSpecialIndentation());
    return classificationCase;
  }
}
