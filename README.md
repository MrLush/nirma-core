## Description

This application provides service for ITMO scientific and research project
(Service of automated norm control and documentation preparation)

## Folders

The main sources are located in src.main.java.ru.domozavr.core, there you can find:

1. configuration there you can find properties classes (e.g. PathsProperties or SecurityProperies)
   SpringFoxConfig - swagger initializer and WebSecurityConfig - it configures http and security params
2. controller - includes controllers for endpoints and GlobalExceptionHandler
3. domain - includes DTO and Entity classes (also some classes for utils)
4. exception - includes custom exceptions
5. mapper - includes mappers
6. repository - includes repositories
7. security - includes classes for security layer (e.g. JWT filter and generator)
8. service - includes service layer (most of the app logic is here)
9. util - includes some utilities, which is using on the service layer
10. validator - includes some classes for validating something (e.g. password)
11. Application - main class which runs the whole app

## Important parameters

You can find all important parameters in src.main.resources.application.yml
Some examples below:
1. spring.datasource includes DB settings
2. spring.servlet defines custom servlet settings (max file size)
3. server.port defines standard application port
4. security includes settings as JWT secret key and expiration time
5. paths - application work directory, paths for utils

## Info for maintenance

You can view existing endpoint documentation using Swagger ({host}/swagger-ui.html), and it automatically adds new
models and controllers.

You can change security for endpoints using annotations or using config params in WebSecurityConfig (look folders s.1)

You can find all existing dependencies in build.gradle file.

If you want to change DB structure (e.g. add tables, constraints or change existing ones) you should use Liquibase. Just
add your SQL scripts to package src.main.resources.db.changelogs.scripts and initialize those scripts in
src.main.resources.db.changelogs.changelog.xml

Also in future releases, it would be great to use docker-compose as containerising tool, you can find samples for it
(Dockerfile, docker-compose.yml, build-docker.sh(or bat))

P.S.: Unit test were generated (and previously tested ofc) using AI (Diffblue), so feel free to modify them.

## How to run this

1. You need JRE 8 or higher installed
2. Created DB with all settings the same as in the app 
(you can find default DB setting in src.main.resources.application.yml -> spring.datasource)

Then just:
java -jar <jar_path>

## Developers

- Daniil Lushnikov

2021
