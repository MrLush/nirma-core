FROM openjdk:8u292-jre-slim

COPY /build/libs/nirma-norm-control.jar nirma-norm-control.jar
EXPOSE 8080

CMD java $JAVA_OPTIONS -jar -Dspring.profiles.active=$SPRING_PROFILE nirma-norm-control.jar
